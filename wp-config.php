<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'memories');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'thematrix');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'lG]Oxt-dJs6{4<%JWPvT#n?V0BI(yby*f+/:$C3 0ygRPw.)i(4~Fa.[0|aC^c&@');
define('SECURE_AUTH_KEY',  'Lq+ia|7I/+t*!094<4 }|es+|rtS917X;8-{9i`KL(B)H0[Ne[4Aa[2y (T#/vy2');
define('LOGGED_IN_KEY',    '?;+)VKda]h(|h0C2<H5afsTj^~hg|{~Zxsdt-4cR,,`B~Q3[I}0Q3hS2DHGc^~hY');
define('NONCE_KEY',        'SiwW+eCTb$|G>?L!aZ~jQ&R5l=7bUP L$;cpE?|u+y}l#|Pux j5@}nX$g1r7HhR');
define('AUTH_SALT',        'Lb!oFpKfnI%zPj23 ,z$FX)9Ko6Dyy3VZy<[n6%k(&Jn<o<L3oMpM#RfGiO|2j2h');
define('SECURE_AUTH_SALT', 'zi1HJUDA+u_O~9%AFZQ;0h.-]S pR<tl%XzFaw{zN-RPbTW/f~2YIhEkH#<+2in ');
define('LOGGED_IN_SALT',   '8hz`^G2@PD@,6]W6_O3Y+^Q-cbdSMx9(b/G0z.(`^g@hw4Fy+5|_W;/g@_tIQY[s');
define('NONCE_SALT',       'I`-b.CGOAUly>6>P%u2K:(_LDE45a>%!D1;%SF%4A+i}1{88QCq+zf>c,-&~W+SZ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

