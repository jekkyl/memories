<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package web2feel
 */
?>

	</div><!-- #content -->
</div>	
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
			Copyright &copy; <?php echo date('Y');?> <a href="<?php bloginfo('url'); ?>" title="<?php bloginfo('name'); ?>"><?php bloginfo('name'); ?></a> - <?php bloginfo('description'); ?> <br/>	
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
        <?php echo "Powered by Webfuzion "?>	
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
